<?php

function db()
{
    if (!isset($db)) {
	// Produktiverser
            $db = new mysqli('localhost', 'web183_10', 'aRandomlyGeneratedPassword', 'id');
	//  $db = new mysqli('localhost', 'root', '', 'id');
        mysqli_set_charset($db, "utf8");
        if (!mysqli_set_charset($db, "utf8")) {
            printf("Error loading character set: %s\n", mysqli_error($db));
        }
    }
    // falls Verbindung nicht zustandekam: Fehler ausgeben
    if (mysqli_connect_errno()) {
        printf("Verbindung fehlgeschlagen: %s\n", mysqli_connect_error());
        exit();
    }
    return $db;
}
