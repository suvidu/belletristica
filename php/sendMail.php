<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'PHPmailer/src/Exception.php';
require 'PHPmailer/src/PHPMailer.php';
require 'PHPmailer/src/SMTP.php';

#erfolg("sendMail-Datei erfolgreich geladen");

function ruoInfoAnSiglinde($ivdKatalognr) {
    $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
    try {
        //Server settings
        $mail->SMTPDebug = 0;                                 // Enable verbose debug output
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'susas.spamsammler@gmail.com';                 // SMTP username
        $mail->Password = 'newsletter';                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to
        //Recipients
        $mail->setFrom('susanne.duncker@immundiagnostik.com', 'Die Datenbank(TM)');
        $mail->addAddress('siglinde.held@immundiagnostik.com', 'Siglinde Held');     // Add a recipient
        $mail->addReplyTo('susanne.duncker@immundiagnostik.com', 'Susanne Duncker');
        $mail->addBCC('susanne.duncker@immundiagnostik.com', 'Susanne Duncker');
        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = "RUO-Version " . getRuoCatalogNo($ivdKatalognr);
        $text = "Liebe Siglinde,<br><br>es wird gerade eine neue RUO-IFU erstellt: " . getRuoCatalogNo($ivdKatalognr);
        $text = $text . "<br>Bitte lege den Kit sowie seine Einzelkomponenten in SAGE an. Vielen Dank!<br><br>Viele Gr&uuml;&szlig;e,<br><br>Susa";
        $mail->Body = $text;
        $mail->send();
        erfolg("Die Nachricht wurde erfolgreich versendet");
    } catch (Exception $e) {
        fehler("Die Nachricht konnte nicht versendet werden. Mailer Error: ", $mail->ErrorInfo);
    }
}

function SusaInformierenPdfUrlFalsch($katalognr) {
    $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
    try {
        //Server settings
        $mail->SMTPDebug = 0;                                 // Enable verbose debug output
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'susas.spamsammler@gmail.com';                 // SMTP username
        $mail->Password = 'newsletter';                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to
        //Recipients
        $mail->setFrom('susanne.duncker@immundiagnostik.com', 'Die Datenbank(TM)');
        $mail->addAddress('susanne.duncker@immundiagnostik.com', 'Susanne Duncker');     // Add a recipient
        $mail->addReplyTo('susanne.duncker@immundiagnostik.com', 'Susanne Duncker');
        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = "Bitte PDF-URL von " . $katalognr . " aktualisieren";
        $mail->Body = "Liebe Susa,<br><br>die PDF-URL des Kits " . $katalognr . " stimmt nicht. Bitte aktualisiere die URL in der Datenbank! Such sie hier: http://www.immundiagnostik.com/en/home/products/products-overview.html<br><br>Danke! Deine Datenbank";
        $mail->send();
    } catch (Exception $e) {
        fehler("Die Information wurde gespeichert, Susa konnte aber nicht informiert werden. Mailer Error: ", $mail->ErrorInfo);
    }
}


function universalkomponentenEingepflegt($ivdKatalognr) {
    $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
    try {
        //Server settings
        $mail->SMTPDebug = 0;                                 // Enable verbose debug output
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'susas.spamsammler@gmail.com';                 // SMTP username
        $mail->Password = 'newsletter';                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to
        //Recipients
        $mail->setFrom('susanne.duncker@immundiagnostik.com', 'Die Datenbank(TM)');
        $mail->addAddress('christopher.seib@immundiagnostik.com', 'Christopher Seib');     // Add a recipient
        $mail->addAddress('lara.rubarth@immundiagnostik.com', 'Lara Rubarth');
        $mail->addReplyTo('susanne.duncker@immundiagnostik.com', 'Susanne Duncker');
	$mail->addBCC('susanne.duncker@immundiagnostik.com', 'Susanne Duncker');
        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = "Universalkomponenten fertig! (" . $ivdKatalognr.")";
        $text = "Liebe Lara,<br>lieber Christopher,<br><br>".$ivdKatalognr." ist jetzt auch mit Universalkomponentenbestellnummern versehen.";
        $text = $text . "<br>Falls eure Etikettendokumente etc. pp noch nicht aktuell sein sollten: jetzt w&auml;re ein guter Zeitpunkt :-)<br><br>Viele Gr&uuml;&szlig;e,<br><br>Susa";
        $mail->Body = $text;
        $mail->send();
        erfolg("Das QM-Team wurde erfolgreich über das Einpflegen der Universalkomponentennummern benachrichtigt");
    } catch (Exception $e) {
        fehler("Die Nachricht konnte nicht versendet werden. Bitte Susa mitteilen, bei welchem Kit das auftrat, und dass das beim Informieren des QM-Teams passiert ist. Danke!<br>Mailer Error: ", $mail->ErrorInfo);
    }
}