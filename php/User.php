<?php

class User
{
    public $username;
    private $userId;
    private $password;
    private $loggedIn;
    private $userrights;
    private $rightIfuEdition;
    private $rightProjects;
    private $rightIntellectualProperty;
    private $rightContracts;
    private $rightDistributors;
    private $rightDBErgaenzung;
    private $rightMolBio;

    function __construct($username)
    {
        $this->username = $username;
        $this->loggedIn = false;
        $this->firstName = "Fremder";
        $this->userrights = array(); // leerer Array
        $this->rightProjects = false;
        $this->rightIntellectualProperty = false;
        $this->rightContracts = false;
        $this->rightDistributors = false;
        $this->rightDBErgaenzung = false;
        $this->rightIfuEdition = false;
        $this->rightMolBio = false;
        $this->updateSession();
    }

    function login($password)
    {
        // TODO Objektorienterier Datenbankzugriff
        $sql = "select immuninet_accounts.id as userId,qm_employee.id as employeeId, immuninet_accounts.username, immuninet_accounts.password, "
            . "qm_employee.name, qm_employee.firstname " .
            "from immuninet_accounts " .
            "left join qm_employee on immuninet_accounts.qm_employeeID=qm_employee.id " .
            "where username='" . $this->username . "'";
        $result = mysqli_query(db(), $sql);
        if ($result) {
            if (mysqli_num_rows($result) == 1) {
                $data = mysqli_fetch_assoc($result);
                $this->firstName = $data['firstname'];
                $this->lastName = $data['name'];
                $this->employeeId = $data['employeeId'];
                $this->userId = $data['userId'];
                $this->password = $data['password'];
                if (md5($password) == $this->password) {
                    $this->loggedIn = true;
                    $_SESSION['userId'] = $this->userId;
                    $this->setUserrights();
                    // Alle Userrechte abfragen und ggf. setzen
                    if ($this->hasRight('Projekte')) {
                        $this->rightProjects = true;
                    }
                    if ($this->hasRight('Schutzrechte')) {
                        $this->rightIntellectualProperty = true;
                    }
                    if ($this->hasRight('IFU-Änderungen')) {
                        $this->rightIfuEdition = true;
                    }
                    if ($this->hasRight('MolBio')) {
                        $this->rightMolBio = true;
                    }
                    if ($this->hasRight('Verträge')) {
                        $this->rightContracts = true;
                    }
                    if ($this->hasRight('Distributoren')) {
                        $this->rightDistributors = true;
                    }
                    if ($this->hasRight('DB-Ergänzung')){
                        $this->rightDBErgaenzung = true;
                    }
                    $this->updateSession();
                    return true;
                } else {
                    return "Falsches Passwort!";
                }
            } elseif (mysqli_num_rows($result) == 0) {
                return "Benutzer nicht gefunden";
            } else {
                return "Datenbankfehler? Benutzer mehrfach in Datenbank vorhanden!";
            }
        } else {
            return "Datenbankfehler! Abfrage: " . $sql . "<br>Fehlermeldung: " . mysqli_error(db());
        }
    }

    function setUserrights()
    {
        // TODO Objektorientierter Datenbankzugriff
        $sql = "SELECT immuninet_accounts.ID as userId, immuninet_accounts.username, immuninet_accounts.qm_employeeID as employeeId, "
            . "immuninet_rights.ID as rightId,Description as userright from immuninet_accountsrightsmapping "
            . "left join immuninet_rights on immuninet_accountsrightsmapping.Immuninet_rights_ID=immuninet_rights.ID "
            . "left join immuninet_accounts on immuninet_accountsrightsmapping.immuninet_accounts_ID=immuninet_accounts.ID "
            . "where immuninet_accounts.username='" . $this->username . "'";
        $result = mysqli_query(db(), $sql);
        if ($result) {
            while ($row = mysqli_fetch_assoc($result)) {
                $this->userrights[] = $row['userright'];
            }
        } else {
            echo "Fehler beim Setzen der Userrechte. Abfrage: " . $sql . "<br>Fehlermeldung: " . mysqli_error(db());
        }
    }

    funct ion hasRight($rightdescription)
    {
        $hasRight = false;
        foreach ($this->userrights as $right) {
            if ($right == $rightdescription) {
                $hasRight = true;
            }
        }
        return $hasRight;
    }

    function updateSession()
    {
        $_SESSION['loggedIn'] = $this->loggedIn;
        $_SESSION['username'] = $this->username;
        $_SESSION['firstName'] = $this->firstName;
        $_SESSION['rightIfuEdition'] = $this->rightIfuEdition;
        $_SESSION['rightMolBio'] = $this->rightMolBio;
        $_SESSION['rightProjects'] = $this->rightProjects;
        $_SESSION['rightIntellectualProperty'] = $this->rightIntellectualProperty;
        $_SESSION['rightContracts'] = $this->rightContracts;
        $_SESSION['rightDistributors'] = $this->rightDistributors;
        $_SESSION['rightDBErgaenzung']=$this->rightDBErgaenzung;
    }
}