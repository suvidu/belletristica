<?php
session_start();

include "php/db.php";
include "php/User.php";
include "php/sendMail.php";

if (isset($_POST['signout'])) {
    unset($user);
    session_unset('loggedIn');
    session_destroy();
}

if (isset($_POST['signin'])) {
    if (isset($_POST['username']) && isset($_POST['password'])) {
        $user = new User($_POST['username']);
        $loginstatus = $user->login($_POST['password']);
        if ($loginstatus !== true) {
            echo $loginstatus;
        }
    }
}
?>

<!DOCTYPE html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Fingerübungen-Verwaltung</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link rel="stylesheet" href="css/custom.css">
</head>
<body>

<?php
require 'navbar.php';
?>

<div class="container-fluid">
    <?php
    if (isset($_GET['main'])) {
        require "Content/" . $_GET['main'] . ".php";
    } else {
        require "Content/home.php";
    }
    ?>
</div>

</body>
</html>