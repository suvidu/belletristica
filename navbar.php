<nav class="navbar navbar-toggleable-sm navbar-light bg-faded">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="?main=home">Fingerübungen</a>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <?php
            if (isset($_SESSION['loggedIn']) && $_SESSION['loggedIn'] && isset($_GET['main'])) {
                // Die folgenden Menüeinträge nur anzeigen, wenn User eingeloggt ist

                // Menüeintrag "Stichwortlisten" nur anzeigen, wenn der User das Recht "stichwoerter" hat
                if ($_SESSION['stichwoerter']) {
                    echo "<li class='nav-item ";
                    // Menüeintrag Stichwörter
                    if ($_GET['main'] == "ifus") {
                        echo "active";
                    }
                    echo "'><a class='nav-link' href='?main=ifus'>Stichwörter</a></li>";
                }
                
                // Menüeintrag "DB-Ergänzung" nur anzeigen, wenn der User das Recht "DB-Ergänzung" hat
                if ($_SESSION['rightDBErgaenzung']) {
                    echo "<li class='nav-item ";
                    // Menüeintrag DB-Ergänzung
                    if ($_GET['main'] == "dbupdate") {
                        echo "active";
                    }
                    echo "'><a class='nav-link' href='?main=dbupdate'>Datenbank-Ergänzung</a></li>";
                }
                
                // Menüeintrag "Molekularbiologie" nur anzeigen, wenn der User das Recht "rightMolBio" hat
                if ($_SESSION['rightMolBio']) {
                    echo "<li class='nav-item ";
                    // Menüeintrag Molekularbiologie
                    if ($_GET['main'] == "molbio") {
                        echo "active";
                    }
                    echo "'><a class='nav-link' href='?main=molbio'>MolBio-IFUs</a></li>";
                }

                // Menüeintrag "Projekte" nur anzeigen, wenn der User das Recht "rightProjects" hat
                if ($_SESSION['rightProjects']) {
                    echo "<li class='nav-item ";
                    // Menüeintrag Projekte
                    if ($_GET['main'] == "projekte") {
                        echo "active";
                    }
                    echo "'><a class='nav-link' href='?main=projekte'>Projekte</a></li>";
                }

                // Menüeintrag "Schutzrechte" nur anzeigen, wenn der User das Recht "rightIntellectualProperty" hat
                if ($_SESSION['rightIntellectualProperty']) {
                    echo "<li class='nav-item ";
                    // Menüeintrag Schutzrechte
                    if ($_GET['main'] == "schutzrechte") {
                        echo "active";
                    }
                    echo "'><a class='nav-link' href='?main=schutzrechte'>Schutzrechte</a></li>";
                }
                
                // Menüeintrag "Verträge" nur anzeigen, wenn der User das Recht "rightContracts" hat
                if ($_SESSION['rightContracts']) {
                    echo "<li class='nav-item ";
                    // Menüeintrag Verträge
                    if ($_GET['main'] == "vertraege") {
                        echo "active";
                    }
                    echo "'><a class='nav-link' href='?main=vertraege'>Verträge</a></li>";
                }
                
                // Menüeintrag "Umfragen" nur anzeigen, wenn der User das Recht "rightSurveys" hat
                if ($_SESSION['rightSurveys']) {
                    echo "<li class='nav-item ";
                    // Menüeintrag Umfragen
                    if ($_GET['main'] == "umfragen") {
                        echo "active";
                    }
                    echo "'><a class='nav-link' href='?main=umfragen'>Umfragen</a></li>";
                }
            }
            ?>
        </ul>
        <?php
        if (isset($_SESSION['loggedIn']) && $_SESSION['loggedIn']) {
            include 'logoutForm.html';
        } else {
            include 'loginForm.html';
        }
        ?>
    </div>
</nav>

